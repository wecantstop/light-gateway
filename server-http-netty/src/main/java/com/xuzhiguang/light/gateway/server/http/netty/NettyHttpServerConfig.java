package com.xuzhiguang.light.gateway.server.http.netty;

import lombok.Data;

/**
 * @author xuzhiguang
 */
@Data
public class NettyHttpServerConfig {

    private Integer port = 8888;

    private String ip = "0.0.0.0";

    // bossEventLoopGroup
    private int bossThreadNum = 1;

    // workerEventLoopGroup
    private int workerThreadNum = Runtime.getRuntime().availableProcessors();

    // 开启 epoll 多路复用
    private boolean enableEpoll = true;

    // body 最大
    private int maxBodyLength = 64 * 1024 * 1024;


}
