package com.xuzhiguang.light.gateway.server.http.netty;

import com.xuzhiguang.light.gateway.common.handler.GatewayRequestHandler;
import com.xuzhiguang.light.gateway.common.server.Server;
import com.xuzhiguang.light.gateway.common.utils.OSUtils;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.HttpServerExpectContinueHandler;
import io.netty.util.concurrent.DefaultThreadFactory;
import lombok.extern.slf4j.Slf4j;

/**
 * @author xuzhiguang
 */
@Slf4j
public class NettyHttpServer extends NettyHttpServerConfig implements Server {

    private final static String BOSS_POOL_NAME = "NettyBoss";

    private final static String WORKER_POOL_NAME = "NettyWorker";

    private final static String SERVER_NAME = "nettyHttp";

    private ServerBootstrap serverBootstrap;

    private EventLoopGroup eventLoopGroupBoss;

    private EventLoopGroup eventLoopGroupWorker;

    private GatewayRequestHandler handler;
    @Override
    public void init(GatewayRequestHandler handler) {

        this.handler = handler;

        this.serverBootstrap = new ServerBootstrap();

        if (userEpoll()) {
            this.eventLoopGroupBoss = new EpollEventLoopGroup(this.getBossThreadNum(), new DefaultThreadFactory(BOSS_POOL_NAME));
            this.eventLoopGroupWorker = new EpollEventLoopGroup(this.getWorkerThreadNum(), new DefaultThreadFactory(WORKER_POOL_NAME));
        } else {
            this.eventLoopGroupBoss = new NioEventLoopGroup(this.getBossThreadNum(), new DefaultThreadFactory(BOSS_POOL_NAME));
            this.eventLoopGroupWorker = new NioEventLoopGroup(this.getWorkerThreadNum(), new DefaultThreadFactory(WORKER_POOL_NAME));
        }
    }

    @Override
    public void start() {

        this.serverBootstrap
                .group(eventLoopGroupBoss, eventLoopGroupWorker)
                .channel(userEpoll() ? EpollServerSocketChannel.class : NioServerSocketChannel.class)
                .option(ChannelOption.SO_BACKLOG, 1024)			//	sync + accept = backlog
                .option(ChannelOption.SO_REUSEADDR, true)   	//	tcp端口重绑定
                .childOption(ChannelOption.TCP_NODELAY, true)   //	该参数的左右就是禁用Nagle算法，使用小数据传输时合并
                .childOption(ChannelOption.SO_SNDBUF, 65535)	//	设置发送数据缓冲区大小
                .childOption(ChannelOption.SO_RCVBUF, 65535)	//	设置接收数据缓冲区大小
                .childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT) // 池化
                .childHandler(new ChannelInitializer<Channel>() {
                    @Override
                    protected void initChannel(Channel channel) throws Exception {
                        channel.pipeline().addLast(
                                new HttpServerCodec(),
                                new HttpObjectAggregator(getMaxBodyLength()),
                                new HttpServerExpectContinueHandler(),
                                new NettyHttpServerHandler(handler)
                        );
                    }
                });

        try {
            this.serverBootstrap
                    .bind(this.getIp(), this.getPort())
                    .sync();
            log.info("Netty Http Server StartUp On Address: " + this.getIp() + ":" + this.getPort());
        } catch (Exception e) {
            throw new RuntimeException("this.serverBootstrap.bind().sync() fail!", e);
        }

    }

    @Override
    public void shutdown() {
        if(eventLoopGroupBoss != null) {
            eventLoopGroupBoss.shutdownGracefully();
        }
        if(eventLoopGroupWorker != null) {
            eventLoopGroupWorker.shutdownGracefully();
        }
    }

    @Override
    public String getName() {
        return SERVER_NAME;
    }

    private boolean userEpoll() {
        return OSUtils.isLinuxPlatform() && Epoll.isAvailable();
    }

}
