<p align="center">
    <b>Light-Gateway</b>
</p>
<p align="center">
	一个基于 netty 实现的多协议、高性能、高扩展性 api 网关
</p>


# 设计图

<img src="doc/设计图.jpg"/>

# 压测

## 对比

服务 	| QPS
----|----
直接访问	|9.15k
SpringCloudGateway	|5.98k
LightGateway	|8.16k

## 服务器部署

|服务| 配置 |
----|----
下游服务| 8C16G  
网关服务| 8C16G  
压测机	| 8C16G  

## 下游服务直接压测

```shell
wrk -t 8 -c 1000 -d 30s --latency --timeout 5s http://172.21.0.4/test.html
Running 30s test @ http://172.21.0.4/test.html
  8 threads and 1000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    38.86ms  150.05ms   4.25s    95.46%
    Req/Sec     9.15k     2.33k   33.64k    89.93%
  Latency Distribution
     50%   13.00ms
     75%   13.40ms
     90%   13.90ms
     99%  719.77ms
  2170642 requests in 30.10s, 511.31MB read
  Socket errors: connect 0, read 2890, write 0, timeout 3
Requests/sec:  72114.82
Transfer/sec:     16.99MB
```

## SpringCloudGateway

```shell
wrk -t 8 -c 1000 -d 30s --latency --timeout 5s http://172.21.0.10:9000/test.html
Running 30s test @ http://172.21.0.10:9000/test.html
  8 threads and 1000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    21.40ms   18.27ms   1.08s    94.54%
    Req/Sec     5.98k   727.44    34.04k    96.71%
  Latency Distribution
     50%   18.24ms
     75%   28.07ms
     90%   36.60ms
     99%   49.92ms
  1428925 requests in 30.10s, 303.90MB read
Requests/sec:  47473.18
Transfer/sec:     10.10MB
```

## Light-Gateway

```shell
wrk -t 8 -c 1000 -d 30s --latency --timeout 5s http://172.21.0.10:9999/test.html
Running 30s test @ http://172.21.0.10:9999/test.html
  8 threads and 1000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    21.60ms   46.49ms 913.00ms   96.65%
    Req/Sec     8.16k     1.65k   60.07k    95.11%
  Latency Distribution
     50%   14.40ms
     75%   14.70ms
     90%   15.53ms
     99%  261.45ms
  1943767 requests in 30.10s, 457.87MB read
Requests/sec:  64577.65
Transfer/sec:     15.21MB
```




