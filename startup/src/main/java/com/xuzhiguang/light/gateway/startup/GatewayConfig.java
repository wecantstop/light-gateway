package com.xuzhiguang.light.gateway.startup;

import com.xuzhiguang.light.gateway.core.route.GatewayRouteConfig;
import lombok.Data;

import java.util.Collections;
import java.util.List;

import static com.xuzhiguang.light.gateway.startup.GatewayConfig.RequestHandlerType.blockingQueue;

/**
 * @author xuzhiguang
 */
@Data
public class GatewayConfig {

    private RequestHandlerType requestHandlerType = blockingQueue;

    private List<GatewayRouteConfig> routes = Collections.EMPTY_LIST;

    public enum RequestHandlerType {
        blockingQueue
    }

}
