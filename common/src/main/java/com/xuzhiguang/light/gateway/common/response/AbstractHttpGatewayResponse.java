package com.xuzhiguang.light.gateway.common.response;

/**
 * @author xuzhiguang
 */
public abstract class AbstractHttpGatewayResponse implements GatewayResponse {

    public abstract int getStatusCode();

}
