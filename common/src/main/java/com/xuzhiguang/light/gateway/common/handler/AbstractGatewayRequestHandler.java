package com.xuzhiguang.light.gateway.common.handler;

import com.xuzhiguang.light.gateway.common.config.AbstractConfigurable;

/**
 * @author xuzhiguang
 */
public abstract class AbstractGatewayRequestHandler<C extends GatewayRequestHandlerConfig> extends AbstractConfigurable<C> implements GatewayRequestHandler<C> {

    protected AbstractGatewayRequestHandler(Class<C> configClass) {
        super(configClass);
    }
}
