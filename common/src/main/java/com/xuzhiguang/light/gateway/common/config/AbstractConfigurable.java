package com.xuzhiguang.light.gateway.common.config;

/**
 * @author xuzhiguang
 */
public abstract class AbstractConfigurable<C> implements Configurable<C> {

    private Class<C> configClass;

    protected AbstractConfigurable(Class<C> configClass) {
        this.configClass = configClass;
    }

    @Override
    public Class<C> getConfigClass() {
        return configClass;
    }

    @Override
    public C newConfig() throws InstantiationException, IllegalAccessException {
        return this.configClass.newInstance();
    }


}
