package com.xuzhiguang.light.gateway.common.utils;

import cn.hutool.core.text.AntPathMatcher;

/**
 * @author xuzhiguang
 */
public class AntPathMatcherUtils {

    private static final AntPathMatcher ANT_PATH_MATCHER = new AntPathMatcher();

    public static boolean match(String pattern, String path) {
        return ANT_PATH_MATCHER.match(pattern, path);
    }

}
