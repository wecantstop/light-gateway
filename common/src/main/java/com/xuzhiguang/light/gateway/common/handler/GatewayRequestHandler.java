package com.xuzhiguang.light.gateway.common.handler;

import com.xuzhiguang.light.gateway.common.config.Configurable;
import com.xuzhiguang.light.gateway.common.config.LoadableConfig;
import com.xuzhiguang.light.gateway.common.config.Nameable;
import com.xuzhiguang.light.gateway.common.request.GatewayRequest;
import com.xuzhiguang.light.gateway.common.request.RequestContext;

/**
 * @author xuzhiguang
 */
public interface GatewayRequestHandler<C extends GatewayRequestHandlerConfig> extends LoadableConfig, Nameable, Configurable<C> {

    String REQUEST_HANDLER_PREFIX = "requestHandler";

    void init(C config);

    void shutdown();

    void handle(GatewayRequest gatewayRequest, RequestContext requestContext);

    @Override
    default String getPrefix() {
        return REQUEST_HANDLER_PREFIX + "." + getName();
    }

}
