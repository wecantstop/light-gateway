package com.xuzhiguang.light.gateway.common.request;

import com.xuzhiguang.light.gateway.common.response.GatewayResponse;

/**
 * @author xuzhiguang
 */
public interface RequestContext {


    void writeAndFlush(GatewayResponse response);


    default void releaseRequest(GatewayRequest gatewayRequest) {

    }


}
