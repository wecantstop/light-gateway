package com.xuzhiguang.light.gateway.common.config;

/**
 * @author xuzhiguang
 */
public interface Nameable {

    String getName();

}
