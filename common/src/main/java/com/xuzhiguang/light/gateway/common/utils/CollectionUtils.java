package com.xuzhiguang.light.gateway.common.utils;


import java.util.Collection;

/**
 * @author xuzhiguang
 */
public class CollectionUtils {

    public static boolean isEmpty(Collection collection) {
        return collection == null || collection.isEmpty();
    }

}
