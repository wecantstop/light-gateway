package com.xuzhiguang.light.gateway.common.response;


import java.util.Map;

/**
 * @author xuzhiguang
 */
public interface GatewayResponse {


    long getResponseTime();

    /**
     * 响应头
     * @return
     */
    Map<String, Object> getHeaders();

    /**
     * 响应体
     * @return
     */
    byte[] getBody();

}
