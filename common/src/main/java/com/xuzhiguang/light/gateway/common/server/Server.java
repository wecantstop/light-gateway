package com.xuzhiguang.light.gateway.common.server;

import com.xuzhiguang.light.gateway.common.config.LoadableConfig;
import com.xuzhiguang.light.gateway.common.config.Nameable;
import com.xuzhiguang.light.gateway.common.handler.GatewayRequestHandler;

/**
 * @author xuzhiguang
 */
public interface Server extends Nameable, LoadableConfig {

    String GATEWAY_SERVERS_PREFIX = "servers";

    void init(GatewayRequestHandler handler);

    void start();

    void shutdown();

    @Override
    default String getPrefix() {
        return GATEWAY_SERVERS_PREFIX + "." + getName();
    }
}
