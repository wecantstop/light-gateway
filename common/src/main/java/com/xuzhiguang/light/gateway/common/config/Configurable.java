package com.xuzhiguang.light.gateway.common.config;

/**
 * @author xuzhiguang
 */
public interface Configurable<C> {

    Class<C> getConfigClass();

    C newConfig() throws InstantiationException, IllegalAccessException;

}

