package com.xuzhiguang.light.gateway.common.utils;

import cn.hutool.system.oshi.OshiUtil;
import oshi.software.os.OperatingSystem;
import oshi.software.os.linux.LinuxOperatingSystem;

/**
 * @author xuzhiguang
 */
public class OSUtils {

    public static boolean isLinuxPlatform() {
        OperatingSystem operatingSystem = OshiUtil.getOs();

        return LinuxOperatingSystem.class.isAssignableFrom(operatingSystem.getClass());
    }

}
