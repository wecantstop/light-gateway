package com.xuzhiguang.light.gateway.common.request;


import java.net.URI;
import java.util.Map;

/**
 * @author xuzhiguang
 */
public interface GatewayRequest {


    /**
     * 请求时间
     * @return
     */
    long getRequestTime();

    /**
     * method
     * @return
     */
    String getMethod();

    /**
     * uri
     * @return
     */
    URI getURI();

    /**
     * 请求头
     * @return
     */
    Map<String, Object> getHeaders();

    /**
     * 请求体
     * @return
     */
    byte[] getBody();

    /**
     * 原始请求
     * @return
     */
    Object getOriginalRequest();

    default boolean needRelease() {
        return false;
    }

}
