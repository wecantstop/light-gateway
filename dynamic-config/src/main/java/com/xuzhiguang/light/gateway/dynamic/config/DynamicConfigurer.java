package com.xuzhiguang.light.gateway.dynamic.config;

import java.util.Properties;

/**
 * @author xuzhiguang
 */
public interface DynamicConfigurer {


    Properties getProperties() throws Throwable;

    String getType();

}
