package com.xuzhiguang.light.gateway.core.filter;

import com.xuzhiguang.light.gateway.core.context.GatewayContext;

/**
 * @author xuzhiguang
 */
public interface GatewayFilter {

    default boolean shouldRun(GatewayContext context) {
        return !context.isError();
    }

    void doFilter(GatewayContext context, GatewayFilterChain chain) throws Throwable;

    default int getOrder() {
        return 0;
    }

}
