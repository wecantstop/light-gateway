package com.xuzhiguang.light.gateway.core.filter;

/**
 * @author xuzhiguang
 */
public abstract class AbstractHttpRoutingGatewayFilter<R, S> extends AbstractRoutingGatewayFilter<R, S> {

    private static final String SCHEME_HTTP = "http";

    private static final String SCHEME_HTTPS = "https";

    @Override
    protected boolean match(String scheme) {
        return SCHEME_HTTP.equals(scheme) || SCHEME_HTTPS.equals(scheme);
    }
}
