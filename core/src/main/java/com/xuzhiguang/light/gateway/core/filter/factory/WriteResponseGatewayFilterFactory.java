package com.xuzhiguang.light.gateway.core.filter.factory;

import com.xuzhiguang.light.gateway.core.filter.EmptyGatewayFilterConfig;
import com.xuzhiguang.light.gateway.core.filter.GatewayFilter;
import com.xuzhiguang.light.gateway.core.filter.WriteResponseGatewayFilter;

/**
 * @author xuzhiguang
 */
public class WriteResponseGatewayFilterFactory extends AbstractGatewayFilterFactory<EmptyGatewayFilterConfig> {

    public WriteResponseGatewayFilterFactory() {
        super(EmptyGatewayFilterConfig.class);
    }
    @Override
    public GatewayFilter apply(EmptyGatewayFilterConfig config) {
        return new WriteResponseGatewayFilter();
    }
}
