package com.xuzhiguang.light.gateway.core.filter;

import com.xuzhiguang.light.gateway.core.context.GatewayContext;

/**
 * @author xuzhiguang
 */
public abstract class AbstractGatewayErrorFilter implements GatewayFilter {

    @Override
    public boolean shouldRun(GatewayContext context) {
        return context.isError();
    }
}
