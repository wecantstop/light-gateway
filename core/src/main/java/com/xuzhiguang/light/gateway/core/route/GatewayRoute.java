package com.xuzhiguang.light.gateway.core.route;

import com.xuzhiguang.light.gateway.core.filter.GatewayFilter;
import lombok.Data;

import java.net.URI;
import java.util.List;

/**
 * @author xuzhiguang
 */
@Data
public class GatewayRoute {


    private final String id;

    private final String path;

    private final URI uri;

    private final List<GatewayFilter> filters;

    private final int order;

    public GatewayRoute(String id, String path, URI uri, List<GatewayFilter> filters, int order) {
        this.id = id;
        this.path = path;
        this.uri = uri;
        this.filters = filters;
        this.order = order;
    }
}
