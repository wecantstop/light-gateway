package com.xuzhiguang.light.gateway.core.filter;

import com.xuzhiguang.light.gateway.core.context.GatewayContext;

/**
 * @author xuzhiguang
 */
public class WriteResponseGatewayFilter implements GatewayFilter {

    private static final int ORDER = -500;

    @Override
    public boolean shouldRun(GatewayContext context) {
        return true;
    }

    @Override
    public void doFilter(GatewayContext context, GatewayFilterChain chain) throws Throwable {

        if (context.isError()) {
            // TODO handle error


        } else {
            context.getRequestContext().writeAndFlush(context.getResponse());
        }
    }

    @Override
    public int getOrder() {
        return ORDER;
    }
}
