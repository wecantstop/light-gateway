package com.xuzhiguang.light.gateway.core.context;

/**
 * @author xuzhiguang
 */
public class GatewayContextConstant {

    public static final String REQUEST_URI_ATTR = "request_uri";

    public static final String REQUEST_METHOD_ATTR = "request_method";

    public static final String REQUEST_HEADERS_ATTR = "request_headers";


    public static final String SERVER_RECEIVED_TIME_ATTR = "server_received_time";

    public static final String SERVER_SEND_TIME_ATTR = "server_send_time";

    public static final String ROUTE_RECEIVED_TIME_ATTR = "route_received_time";

    public static final String ROUTE_SEND_TIME_ATTR = "route_send_time";

}
