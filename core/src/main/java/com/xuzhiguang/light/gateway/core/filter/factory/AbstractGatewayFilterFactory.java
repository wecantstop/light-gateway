package com.xuzhiguang.light.gateway.core.filter.factory;

import com.xuzhiguang.light.gateway.common.config.AbstractConfigurable;
import com.xuzhiguang.light.gateway.core.filter.GatewayFilterConfig;

/**
 * @author xuzhiguang
 */
public abstract class AbstractGatewayFilterFactory<C extends GatewayFilterConfig> extends AbstractConfigurable<C> implements GatewayFilterFactory<C> {

    private final static String GATEWAY_FILTER_FACTORY_SUFFIX = "GatewayFilterFactory";

    protected AbstractGatewayFilterFactory(Class<C> configClass) {
        super(configClass);
    }

    @Override
    public String getName() {
        String factoryFullName = this.getClass().getSimpleName();
        return factoryFullName.substring(0, factoryFullName.length() - GATEWAY_FILTER_FACTORY_SUFFIX.length());
    }
}
