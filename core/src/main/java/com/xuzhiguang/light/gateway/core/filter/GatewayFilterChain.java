package com.xuzhiguang.light.gateway.core.filter;

import com.xuzhiguang.light.gateway.core.context.GatewayContext;

/**
 * @author xuzhiguang
 */
public interface GatewayFilterChain {

    void doFilter(GatewayContext context);

}
