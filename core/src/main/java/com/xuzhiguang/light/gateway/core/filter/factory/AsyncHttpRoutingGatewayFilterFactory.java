package com.xuzhiguang.light.gateway.core.filter.factory;

import com.xuzhiguang.light.gateway.core.filter.AsyncHttpRoutingGatewayFilter;
import com.xuzhiguang.light.gateway.core.filter.GatewayFilter;
import org.asynchttpclient.AsyncHttpClientConfig;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClientConfig;

/**
 * @author xuzhiguang
 */
public class AsyncHttpRoutingGatewayFilterFactory extends AbstractHttpRoutingGatewayFilterFactory {

    @Override
    public GatewayFilter apply(AbstractHttpRoutingGatewayFilterFactory.Config config) {

        AsyncHttpClientConfig httpClientConfig = new DefaultAsyncHttpClientConfig.Builder()
                .setConnectTimeout(config.getConnectTimeout())
                .setReadTimeout(config.getConnectTimeout())
                .setReadTimeout(config.getReadTimeout())
                .setMaxRequestRetry(config.getMaxRequestRetry())
                .build();

        return new AsyncHttpRoutingGatewayFilter(new DefaultAsyncHttpClient(httpClientConfig));
    }
}
