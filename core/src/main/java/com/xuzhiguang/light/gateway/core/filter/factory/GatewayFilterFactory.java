package com.xuzhiguang.light.gateway.core.filter.factory;

import com.xuzhiguang.light.gateway.common.config.Configurable;
import com.xuzhiguang.light.gateway.common.config.Nameable;
import com.xuzhiguang.light.gateway.common.config.LoadableConfig;
import com.xuzhiguang.light.gateway.core.filter.GatewayFilter;
import com.xuzhiguang.light.gateway.core.filter.GatewayFilterConfig;

/**
 * @author xuzhiguang
 */
public interface GatewayFilterFactory<C extends GatewayFilterConfig> extends Nameable, LoadableConfig, Configurable<C> {

    String GATEWAY_GLOBAL_FILTER_PREFIX = "globalFilters";

    GatewayFilter apply(C config);

    @Override
    default String getPrefix() {
        return GATEWAY_GLOBAL_FILTER_PREFIX + "." + getName();
    }
}
