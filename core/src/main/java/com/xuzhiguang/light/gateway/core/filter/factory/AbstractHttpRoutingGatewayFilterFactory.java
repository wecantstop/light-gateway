package com.xuzhiguang.light.gateway.core.filter.factory;

import com.xuzhiguang.light.gateway.core.filter.GatewayFilterConfig;
import lombok.Data;

/**
 * @author xuzhiguang
 */
public abstract class AbstractHttpRoutingGatewayFilterFactory extends AbstractGatewayFilterFactory<AbstractHttpRoutingGatewayFilterFactory.Config> {

    protected AbstractHttpRoutingGatewayFilterFactory() {
        super(AbstractHttpRoutingGatewayFilterFactory.Config.class);
    }

    @Data
    public static class Config implements GatewayFilterConfig {

        private int connectTimeout = 3000;

        private int requestTimeout = 3000;

        private int readTimeout = 3000;

        private int maxRequestRetry = 0;

    }

}
