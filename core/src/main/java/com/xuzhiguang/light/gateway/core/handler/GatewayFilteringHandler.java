package com.xuzhiguang.light.gateway.core.handler;

import com.xuzhiguang.light.gateway.core.context.GatewayContext;
import com.xuzhiguang.light.gateway.core.filter.DefaultGatewayFilterChain;
import com.xuzhiguang.light.gateway.core.filter.GatewayFilter;
import com.xuzhiguang.light.gateway.core.filter.GatewayFilterChain;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xuzhiguang
 */
public class GatewayFilteringHandler {

    private final List<GatewayFilter> globalFilters;

    public GatewayFilteringHandler(List<GatewayFilter> globalFilters) {
        this.globalFilters = globalFilters;
    }

    public void handle(GatewayContext context) {

        List<GatewayFilter> combined = new ArrayList<>(this.globalFilters);

        if (context.getRoute() != null) {
            combineAndSort(combined, context.getRoute().getFilters());
        }

        GatewayFilterChain filterChain = new DefaultGatewayFilterChain(combined);
        filterChain.doFilter(context);
    }

    private void combineAndSort(List<GatewayFilter> filters1, List<GatewayFilter> filters2) {
        // TODO combine
    }

}
