package com.xuzhiguang.light.gateway.core.filter.factory;

/**
 * @author xuzhiguang
 */
public class GatewayFilterFactoryLoader {

    private static final String PATTEN = "com.xuzhiguang.light.gateway.core.filter.factory.%sGatewayFilterFactory";

    public static GatewayFilterFactory load(String name) {

        GatewayFilterFactory factory = null;
        try {
            Class c = Class.forName(String.format(PATTEN, name));
            factory = (GatewayFilterFactory) c.newInstance();
        } catch (Exception ignored) {

        }
        return factory;
    }
}
