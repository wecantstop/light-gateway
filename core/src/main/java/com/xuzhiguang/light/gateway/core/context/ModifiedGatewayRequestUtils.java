package com.xuzhiguang.light.gateway.core.context;


import java.net.URI;
import java.util.Map;

/**
 * @author xuzhiguang
 */
public class ModifiedGatewayRequestUtils {


    public static long getRequestTime(GatewayContext context) {
        return context.getRequest().getRequestTime();
    }


    public static String getMethod(GatewayContext context) {
        return (String) context.getAttributeOrDefault(GatewayContextConstant.REQUEST_METHOD_ATTR, context.getRequest().getMethod());
    }

    public static URI getURI(GatewayContext context) {
        return (URI) context.getAttributeOrDefault(GatewayContextConstant.REQUEST_URI_ATTR, context.getRequest().getURI());
    }

    public static Map<String, Object> getHeaders(GatewayContext context) {
        return context.getRequest().getHeaders();
    }

    public static byte[] getBody(GatewayContext context) {
        return context.getRequest().getBody();
    }
}
