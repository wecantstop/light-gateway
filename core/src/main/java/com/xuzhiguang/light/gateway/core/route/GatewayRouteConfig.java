package com.xuzhiguang.light.gateway.core.route;

import lombok.Data;

import java.util.Collections;
import java.util.Map;
import java.util.Properties;

/**
 * @author xuzhiguang
 */
@Data
public class GatewayRouteConfig {

    private String id;

    private String path;

    private String uri;

    private int order;

    private Map<String, Properties> filters = Collections.EMPTY_MAP;

}
